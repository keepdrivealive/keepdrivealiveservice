module gitlab.com/keepDriveAlive/keepDriveAliveService

go 1.15

require (
	golang.org/x/sys v0.0.0-20210124154548-22da62e12c0c
	gopkg.in/gcfg.v1 v1.2.3
	gopkg.in/warnings.v0 v0.1.2 // indirect
)
