// +build windows

package main

import (
	"fmt"
	"os"
	"syscall"
	"time"
)

// keepDriveAlive will keep the drive active and alive
//  by open, write a timestamp at the root of the drive
//  (a hidden file) and close it.
func keepDriveAlive() {
	for _, l := range cfg.Drives.Letter {
		kdaPathTMP := l + ":\\.keepDriveAlive"

		if _, err := os.Stat(kdaPathTMP); os.IsNotExist(err) {
			// path does not exist
			f, err := createHiddenFile(kdaPathTMP)
			if err != nil {
				elog.Error(1, fmt.Sprintf("could not write to drive %s with error :\n\t%v", l, err))
			}
			_, err = f.Write([]byte(fmt.Sprintf("%v", time.Now().Unix())))
			if err != nil {
				elog.Error(1, fmt.Sprintf("could not write to file %s with error :\n\t%v", kdaPathTMP, err))
			}
			f.Close()
		} else {
			// path exist, so update timestamp
			af, err := os.OpenFile(kdaPathTMP, os.O_WRONLY|os.O_TRUNC, 0666)
			if err != nil {
				elog.Error(1, fmt.Sprintf("could not write to file %s with error :\n\t%v", kdaPathTMP, err))
			}
			af.Write([]byte(fmt.Sprintf("%v", time.Now().Unix())))
			af.Close()
		}
	}
}

func createHiddenFile(name string) (*os.File, error) {
	nameptr, err := syscall.UTF16PtrFromString(name)
	if err != nil {
		return nil, err
	}
	f, err := os.Create(name)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	err = syscall.SetFileAttributes(nameptr, syscall.FILE_ATTRIBUTE_HIDDEN)
	if err != nil {
		os.Remove(name)
		return nil, err
	}

	return f, nil
}

func cleanUp() {
	// remove every created file
	for _, l := range cfg.Drives.Letter {
		kdaPathTMP := l + ":\\.keepDriveAlive"
		if err := os.Remove(kdaPathTMP); err != nil {
			elog.Error(2, fmt.Sprintf("could not delete in %v, %v", cfg.Drives.Letter, err))
		}
	}
}
